from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import ManagerUser
# Create your views here.

def index(request):
    users = ManagerUser.objects.all()
    template = loader.get_template('MainModule/index.html')
    context = {
        'users': users,
    }
    return HttpResponse(template.render(context, request))