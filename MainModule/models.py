from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class ManagerUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    Company = models.CharField(max_length=100)